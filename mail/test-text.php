<?php

/* @var $this yii\web\View */
/* @var $user bridgeinn\pluto\models\User */
/* @var $link string */

?>
<?= Yii::t('pluto', 'Hello {username},', [
    'username' => $user->name
]) ?>

<?= Yii::t('pluto', 'This is a test email:') ?>
